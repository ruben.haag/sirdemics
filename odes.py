
@njit
def periodicAlpha(t, partIn, periods, R_values, beta):
    R_in, R_out = R_values
    daysInPeriod = np.mod(t, periods)
    # Positive, if in Lockdown
    daysInLockdown = (daysInPeriod - np.outer(1-partIn, period)).flatten()
    isLockdown = daysInLockdown > 0
    Rs = np.ones(daysInLockdown.shape)
    Rs[isLockdown] = np.exp(-lamda * (daysInLockdown[isLockdown]+1)) + R_in
    Rs[np.logical_not(isLockdown)] = R_out
    return Rs.T*beta

@njit
def sir_variableAlpha(t, p, alpha_calc, Rts, beta, partIn, period):
    x = p[:l*l]
    y = p[l*l:2*l*l]
    z = p[2*l*l:]
    
    alpha = alpha_calc(t, partIn, period, Rts, beta)
    
    dx = - alpha * x * y
    dz = beta *y
    dy = -(dx+dz)
    
    return np.concatenate((dx,dy,dz))

@njit
def sir(t, p, alpha, beta):
    x, y, z = p
    
    dx = - alpha * x * y
    dz = beta *y
    dy = -(dx+dz)
    
    return np.array(dx,dy,dz)

def sir_testing(t, p, alpha, beta, phi):
    x, y0, y1, z = p
    y = np.array(y0, y1)
    dx = - x * np.dot(y, alpha)
    dy1 = x * np.dot(y, alpha*phi) - beta * y[0] + 
    dy2 = x * np.dot(y, alpha*(1-phi)) - beta * y[1]