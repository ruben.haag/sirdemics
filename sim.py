'''
This code Implements and numerically solves the ode system we defined in order to model the influence, which vaccinations have on a pandemic.
It saves its results in three numpy arrays which one for the number of peaks, which can then be used by plot_map.ipynb to generate a stability diagram
It wll also directly generate a stability diagram, when executed.


@author: Ruben Haag, Jannis Fischer, Katharina Sophie Wolf, Daniel Schöning
@Tutors: Joel Wagner, Simon Bauer
@contact: ruben.haag@stud.uni-goettingen.de


This file was created as part of the seminar "Epdiemics, Infodemics and Mobility" at the University of Goettingen during the summer semester of 2022.
'''



############### Imports ###############################################################################

from numba import njit			# Numba can do just in time compilation and thus can significantly speed up the ODE for integration
import numpy as np			# Numpy is a/the general purpose math module in python
from scipy.integrate import solve_ivp 	# Integrator used to integrate the odes
from scipy.signal import find_peaks	# Peak finder used to figure out, wether the ODES reaches a stable fixpoint at the end of the integration or is still changing, uses multithreading
from itertools import product		# Allows us to make a list, that combines every value for T with every p_base
import matplotlib.pyplot as plt         # plotting and Grafical display
#######################################################################################################



############### Define Parameters #####################################################################
l = 300     # Pixels 		- The resulting image will have l*l pixels                        
# !!!!!!!! WARNING for l=300 the code will need approximatly 16GB of RAM choose l=100 for lower ram usage!!!!!!!!


beta0 = 0.5 #days^-1		- Infection rate
gamma = 0.1 #days^-1		- Recovery rate
nu = 1/100 #days^-1			- Rate, at which people go from removed to susceptible
T_range = [1,100] #days 	- Range in which the mean memory time T is varied with l steps
p_base_range = (0.01, 0.4)# - Range in which the maximal reduction of infection rate (1/p_base) is varied with l steps

p_cap = 1e-3	 # Risk, at which at which p_base is reached
epsilon = 1e-4   # 

maxVax = 0.01    #  Maximale Impfrate

p_vax = 1000     #  Anteil der Leute, die angst haben und sich deswegen Impfen lassen wollen
mu_im = 1/14     #  1/days
#######################################################################################################



############# Make list, that combines every T with every p_base ######################################

# I know, that it is not perfect, but it works and this part is not time critical
T = np.linspace(T_range[0], T_range[1], num=l)
p_base = np.linspace(p_base_range[0], p_base_range[1], num=l, endpoint=True)
params = np.array(list(product(T, p_base))).T
T = params[0]
p_base = params[1]
#######################################################################################################



################## Functions needed by the odes ######################################################

'''
Reduction of infection Rate based on perceived Risk H
@parameters
- Risk H
- maximal reduction of infection rate (1/p_base)
- Risk p_cap, at which at which p_base is reached
- epsilon
'''
@njit # Precompile function to speed up performance
def P(H, p_base=p_base, p_cap=p_cap, epsilon=epsilon):
    return p_base + (1-p_base)/p_cap * epsilon * np.log(1+np.exp(1/epsilon * (p_cap - H)))



'''
actual Vaccination rate
@parameters:
- Risk H
- Maximal vaccination rate maxVax
@returns
actual vaccination rate (1- np.exp(-p_vax*H))* maxVax
'''
@njit # Precompile function to speed up performance
def vax(H, maxVax=maxVax):
    return (1- np.exp(-p_vax*H))* maxVax

'''
Seasonality (Not used)
models the effects of the season on covid
@parameters:
- t(Float) time at which to evaluea
@returns:
- 1+ s*  cos(omega*t) with s being a global variable that defines the seasonal effect
'''
@njit # Precompile function to speed up performance
def Gamma(t):
    return 1+ s* np.cos(omega*t)



####### Define ODES ######################################################################
'''
ODES, as presented in our talk

'''

@njit # Precompile function to speed up performance
def odes(t, p, T, p_base):
	####### Split up array into the the single compartments #######
    S = p[:l*l]				# Susceptible
    V = p[l*l:2*l*l]		# Vaccinated and Imune
    V1 = p[2*l*l:3*l*l]		# Vaccinated and still susceptible
    I = p[3*l*l:4*l*l]		# Infectuos
    H_1 = p[4*l*l:5*l*l] 	# memory kernel
    H = p[5*l*l:6*l*l]		# Perceived Risk
    R = p[6*l*l:7*l*l]		# Removed (imune)

    # Perform integration step for all compartments using the ODES defined in our talk
    dS = - beta0 * P(H) * I * S + nu * (R+V) - vax(H) * S 	# dS/dT
    dV =  V1 * mu_im - nu * V 											# dV/dT
    dV1 = vax(H) * S - V1 * mu_im - beta0 * P(H) * I * V1	# dV1/dT
    dI = beta0 * P(H) * I * (S+V1) - gamma * I 				# dI/dT
    dR = gamma*I - nu*R													# dR/dT
    dH_1 = 2/T * (I - H_1)												# dH_1/dT
    dH = 2/T * (H_1 - H)												# dH/dT
    
    # Put all compartments into on array again
    return np.concatenate((dS,dV, dV1, dI,dH_1, dH, dR))


###################  Define Starting parameters so that they are close to a fixpoint ####################

I0 = 1e-4 * np.ones(l*l)	# Infetion Rate
H_10 = 0 * np.ones(l*l)		# Memory Kernel
H0 = 0 * np.ones(l*l)		# perceived Risk
V0 = 0.36 *np.ones(l*l)		# Vaccinated and immune
V10= 0.04 *np.ones(l*l)		# Vaccinated and not yet immune
R0 = 0.02 * np.ones(l**2)	# Removed
S0 = (1-I0-V0-R0-V10)		# Susceptible chosen, so that total population size is normalized to 1


# Put starting values into a single array, whicht solve_ivp can use
p0 = np.concatenate((S0, V0, V10, I0, H_10, H0, R0))
#########################################################################################################



################## Time range, in which to simulate ####################################################

start = 0.        	# Start time of simulations
end = 100000      	# End time of simulation #### Can be shortened to increase speed
max_range = 6000  	# Save only timepoints from end-max_range #### Will decrease RAM usage if this is chosen smaller
dt_step = 2       	# Step size, at which to save

maxdt = 1			# Maximum Time step
#########################################################################################################



################## Simulation ###########################################################################
# Simulate using solve IVP
sol = solve_ivp(odes, (start, end), p0, max_step=maxdt, args=(T,p_base), t_eval=range(end-max_range,end, dt_step))

p = sol.y
ts = sol.t

# Save simulation Results
name = "Results:300x300"		# Name in Which to save
np.save(f"{name}_p", p) 		# Save Compartments
np.save(f"{name}_times", ts)	# Save timesteps

# Split results into single compartments
Ss = p[:l*l]			# Susceptible
Vs = p[l*l:2*l*l]		# Vaccinated and Immune
V1s = p[2*l*l:3*l*l]	# Vaccinated and Susceptible
Is = p[3*l*l:4*l*l]		# Infectios
H_1s= p[4*l*l:5*l*l]	# Memory Kernel
Hs = p[5*l*l:6*l*l]		# Perceived Risk
Rs = p[6*l*l:7*l*l]		# Removed


##########################################################################################################






#################### Find peaks for every single ode system and save them in karte ##############

karte = np.zeros((l,l)) # Define Peak map
for n in range(l):
    for m in range(l):
        i = n*l+m # Array index
        t = ts	  # Times, only nessesacry if distance between peaks is also counted
        S = Ss[i]
        V = Vs[i]
        V1 = V1s[i]
        I = Is[i]
        H_1 = H_1s[i]
        H = Hs[i]
        R = Rs[i]
        peaks, properties = find_peaks(I, height=1e-16)


        if len(peaks)>0:
            karte[-n-1, m] = len(peaks) # Save the number of peaks found for this set of parameters into karte
        else:
            karte[-n-1, m] = -1

# Save the peak map
np.save(f"{name}_karte", karte)
################################################################################################





###### Plotting ###############################################################################
fig, ax = plt.subplots(figsize=(6,6))
plt.imshow(karte)
plt.yticks([0,1/2*l,l], [t_range[1],(t_range[1]-t_range[0])/2 + t_range[0],t_range[0]])
plt.xticks([0, 1/3*l, 2/3*l, l], [p_base_range[0],(p_base_range[1]-p_base_range[0])/3 + p_base_range[0],(p_base_range[1]-p_base_range[0])*2/3 + p_base_range[0],p_base_range[1]])
plt.colorbar()


####### Plot Expected Value for p_base #######
R0 = 5
vax = 0.4
Rvax = R0 * (1-vax)
p_base = 1/Rvax
plt.vlines(p_base*l/p_base_range[1], 0, l-1, colors='r', lw=5 * 300/l)


##### Save plot ####
plt.savefig(f"{name}.png")
########################################################################################





