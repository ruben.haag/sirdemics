from scipy.linalg import eigvals
from scipy.optimize import root
import numpy as np

beta0 = 0.5 #days^-1
gamma = 0.1 #days^-1
nu = 1/100 #days^-1
p_cap = 1e-3
epsilon = 1e-4
maxVax = 0.01 # Von N0 können pro tag geimpft werden

p_vax = 1000 # Zahl der Leute(*), die angst haben und sich deswegen Impfen lassen wollen
mu_im = 1/14 #1/days

def softplus(H,pbase,pcap,pepsilon):
    slope = (1-pbase)/pcap
    return slope*pepsilon*np.log(np.exp(1/pepsilon*(pcap-H))+1)+pbase

def softplus_deriv(H,pbase,pcap,pepsilon):
    slope = (1-pbase)/pcap
    return -slope*np.exp(1/pepsilon*(pcap-H))/(1+np.exp(1/pepsilon*(pcap-H)))

def vax(H,p_base,p_cap,epsilon):
    return (1- np.exp(-p_vax*H))* maxVax

def dVax(H,p_base,p_cap,epsilon):
    return p_vax*np.exp(-p_vax * H)*maxVax

def FP_I(I, *args):
    b = args[0]
    o = args[1]
    g = args[2]
    pcap = args[3]
    pbase = args[4]
    pepsilon = args[5]
    return b*I+o/softplus(I,pbase,pcap,pepsilon)-b*o/g*(1-I)

def FPparams(p,iguess=0.001):
    p_cap = p['pcap']
    p_base = p['pbase']
    epsilon = p['pepsilon']
    b = p['beta']
    o = p['nu']
    g = p['gamma']
    
    sol = root(FP_I,iguess, args=(b,o,g,p_cap,p_base,epsilon))

    ISTAR = sol.x[0]
    RSTAR = g/o*ISTAR
    SSTAR = (1-ISTAR)/(vax(ISTAR,p_base,p_cap,epsilon)/(mu_im+b*softplus(ISTAR,p_base,p_cap,epsilon)*ISTAR+o)+b*softplus(ISTAR,p_base,p_cap,epsilon)*ISTAR/o-1)
    
    V1STAR = (vax(ISTAR,p_base,p_cap,epsilon)-SSTAR)/(mu_im+b*vax(ISTAR,p_base,p_cap,epsilon)*ISTAR)
    
    VSTAR = mu_im/o*V1STAR
    
    return SSTAR,ISTAR,RSTAR, VSTAR, V1STAR

def jacobian(p):
    p_cap = p['pcap']
    p_base = p['pbase']
    epsilon = p['pepsilon']
    beta0 = p['beta']
    nu = p['nu']
    gamma = p['gamma']
    T = p['tau']
    H = p['H']
    H1 = p['H1']
    
    S,I,R,V,V1 = FPparams(p)
    P_H = softplus(I,p_base,p_cap,epsilon)
    dP_H = softplus_deriv(I,p_base,p_cap,epsilon)

    dSdS = -beta0 * P_H * I - vax(I,p_base,p_cap,epsilon)
    dSdV1 = 0
    dSdV = nu
    dSdI = -beta0 * P_H * S 
    dSdR = nu
    dSdH1 = 0
    dSdH = -beta0 * I *S * dP_H - S * dVax(I,p_base,p_cap,epsilon)


    dV1dS = vax(I,p_base,p_cap,epsilon)
    dV1dV1 = -mu_im - beta0 * P_H * I
    dV1dV = 0
    dV1dI = -beta0 * P_H * V1
    dV1dR = 0
    dV1dH1 = 0
    dV1dH = -beta0 * I * V1 * dP_H + S * dVax(I,p_base,p_cap,epsilon)


    dVdS = 0
    dVdV1 = mu_im
    dVdV = -nu
    dVdI = 0
    dVdR = 0
    dVdH1 = 0
    dVdH = 0


    dIdS = beta0 * P_H * I
    dIdV1 = beta0 * P_H * I
    dIdV = 0
    dIdI = -gamma + beta0 * P_H * (S + V1)
    dIdR = 0
    dIdH1 = 0
    dIdH = beta0 * I * (S+V1) * dP_H


    dRdS = 0
    dRdV1 = 0
    dRdV = 0
    dRdI = gamma
    dRdR = -nu
    dRdH1 = 0
    dRdH = 0

    dH1dS = 0
    dH1dV1 = 0
    dH1dV = 0
    dH1dI = 2/T
    dH1dR = 0
    dH1dH1 = -2/T
    dH1dH = 0

    dHdS = 0
    dHdV1 = 0
    dHdV = 0
    dHdI = 0
    dHdR = 0
    dHdH1 = 2/T
    dHdH = -2/T

    ret = np.array([
        [dSdS ,dSdV1, dSdV ,dSdI ,dSdR ,dSdH1,dSdH],
        [dV1dS ,dV1dV1,dV1dV ,dV1dI ,dV1dR ,dV1dH1, dV1dH],
        [dVdS, dVdV1,dVdV ,dVdI ,dVdR ,dVdH1,dVdH],
        [dIdS, dIdV1,dIdV ,dIdI ,dIdR ,dIdH1,dIdH],
        [dRdS, dRdV1,dRdV ,dRdI ,dRdR ,dRdH1,dRdH],
        [dH1dS, dH1dV1, dH1dV, dH1dI, dH1dR, dH1dH1, dH1dH],
        [dHdS, dHdV1, dHdV, dHdI, dHdR ,dHdH1, dHdH]
        ])
    return ret

def largestEW(p):
    J = jacobian(p)
    EW = eigvals(J)
    return np.max(EW.real)
