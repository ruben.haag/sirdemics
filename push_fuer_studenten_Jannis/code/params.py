import numpy as np

p={
    'y0':[1-0.001, 0.001, 0, 0.00, 0.00],
    'beta': 0.5,
    'gamma':1/10,
    'nu':1/100,
    'tau': 45,
    'pbase': 0.17,
    'pcap':1/1000,
    'pepsilon':0.0001,
    's': 0.2,
    'w':2*np.pi/360,
    'tmin': 0,
    'tmax': 3000,
    'stepsize': 0.1,
    'maxstep':10,
    'direction':True,
    'threshold': 0.003,
}