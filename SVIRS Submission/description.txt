Code and presentation from Ruben Haag, Jannis Fischer, Katharina Wolf and Daniel Schöning

Topic: SVIRS Model - reproducing Joel's Bachelor Thesis and implementing vaccination
Project 5. SIR dynamics, behavioral feedback and emergent chaotic dynamics
Tutor: Joel Wagner, Simon Bauer